<!DOCTYPE html>

<?php
// variables globales
$numero1 = $_POST['numero1'];
$numero2 = $_POST['numero2'];
$operacion = $_POST['operacion'];
if ($operacion == "+") {
    $suma=$numero1+$numero2;
    $resultado = $suma;
} elseif($operacion=='-') {
    $resta=$numero1-$numero2;
    $resultado = $resta;
}elseif ($operacion=="*") {
    $multi=$numero1*$numero2;
    $resultado = $multi;
}elseif($operacion=="/"){
    $divi=$numero1/$numero2;
    $resultado = $divi;
}


?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-item nav-link active" href="suma.php">Calculadora<span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="mayoraMenor.php">Mayor a menor</a>
            <a class="nav-item nav-link" href="#¿ciclofor.php">ciclo for </a>
          </div>
        </div>
      </nav>
    <form id="form1" name="form1" method="POST" action="">
    <p>Numero 1</p>
    <input type="text" name="numero1" id="numero1" value="<?=$numero1?>">
    <select name="operacion" id="operacion" class="form-control">
        <option value="+" id="+" name="+">+</option>
        <option value="-" id="-" name="-">-</option>
        <option value="*" id="*" name="*">*</option>
        <option value="/" id="/" name="/">/</option>
    </select>
    <p>Numero 2</p>
    <input type="text" name="numero2" id="numero2" value="<?=$numero2?>">
    <p>Resultado</p>
    <input type="text" name="resultado" id="resultado" value="<?=$resultado?>">
    <input type="submit" name="calcular" id="calcular" value="Calcular">
    </form>
</body>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
</html>